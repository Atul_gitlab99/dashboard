import React,{Component} from 'react';
import './Card.css';
import BarChart from './BarChart';
export default class Card extends Component{
    render=()=>{
        return(
            <div>
        <section class="upper-grid">
            <div class="card">
                <div class="reviews">
                    <h1>1,281</h1>
                    <p>You got +23.5% more reviews</p>
                    <section class="posi-nega">
                        <div class="card">
                            <div class="posi">
                                <h5>+264</h5>
                            </div>
                        </div>
                        <div class="card">
                            <div class="nega">
                            <h5>-12</h5>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="card">
                <div class="rating">
                    <h2>Average Rating</h2>
                    <p>Rating Weekly</p>
                </div>
            </div>
            <div class="card">
                <div class="analysis">
                    <div class="posi-rev">
                        <h4>--Positive Reviews--</h4>
                    </div>
                    <div class="neu-rev">
                        <h4>--Neutral Reviews--</h4>
                    </div>
                    <div class="nega-rev">
                        <h4>--Negative Reviews--</h4>
                    </div>
                </div>
            </div>
        </section>
        <section class="lower-grid">
            <div class="card">  
                <div class="visitors">
                    <BarChart />
                </div>
            </div>
        </section>
        </div>
        );
    }
}