import React, { Component } from 'react';
import Card from './Card.js';
import './Nav.css';
//import {Star} from '@material-ui/icons/StarBorder';
export default class Nav extends Component{
    render=()=>{
        return(
        <div class="row">
            <div class="column-1">    
                <section className="side-bar">
                        <div className="nav">
                            <div id="row-1">Converse Store</div>
                            <div id="row-2"><a className="current"href="#">DASHBOARD</a></div>
                            <div id="row-3"><a href="#">WIDGET</a></div>
                            <div id="row-4"><a href="#">REVIEWS</a></div>
                            <div id="row-5"><a href="#">CUSTOMERS</a></div>
                            <div id="row-6"><a href="#">ONLINE ANALYSIS</a></div>
                            <div id="row-7"><a href="#">SETTINGS</a></div>
                        </div>
                </section>
            </div>
            <div class="column-2">
                <Card/>
            </div>
        </div>
        );  }   
}        
